#include 	<cstdio>
#include	<iostream>
#include	<cmath>
#include 	<cstring>
#include 	<cstdlib>
#include 	<vector>
#include 	<string>
#include 	<algorithm>
#include 	<queue>
#include 	<deque>
#include 	<set>
#include 	<stack>
#include 	<map>
#include 	<sstream>
#include 	<ctime>
#include	<iomanip>
#include 	<functional>

#define 	Time 		printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K) 	for(ll J=R;J<K;++J)
#define 	Rep(I,N) 	For(I,0,N)
#define 	MP 			make_pair
#define 	ALL(X) 		(X).begin(),(X).end()
#define 	SF 			scanf
#define 	PF 			printf
#define 	pii 		pair<long long,long long>
#define 	pdd 		pair<double , double>
#define 	Sort(v) 	sort(ALL(v))
#define 	Test 		freopen("a.in","r",stdin)
#define 	Testout 	freopen("a.out","w",stdout)
#define 	pb 			push_back
#define 	Set(a,n) 	memset(a,n,sizeof(a))
#define 	MAXN 		100000+99
#define 	EPS 		1e-15
#define 	inf 		1ll<<62

typedef long long ll;

using namespace std;
//
bool visited[10000] = { false };
bool F = false;
bool u = false;
string currentmosh = "", current = "", amalgar = "^*/+-sctql";
bool makhsefr = false;
ll h = -1;
ll g = 0;

inline int check(int q = 0)
{
	static int f = 0;
	if (q == 0)
	{
		for (int i = 0; i < currentmosh.length(); i++)
		{
			if (currentmosh[i] == '(')
			{
				int k = 1;
				for (int j = i + 1; ; j++)
				{
					if (currentmosh[j] == '(')
						k++;
					if (currentmosh[j] == ')')
						k--;
					if (k == 0)
					{
						i = j;
						break;
					}
				}
				continue;
			}
			if (currentmosh[i] == '+' || currentmosh[i] == '-' || currentmosh[i] == '*' || currentmosh[i] == '/' || currentmosh[i] == '^')
				f++;
		}
	}
	if (f == 1 || f == 0)
	{
		f = 0;
		return 0;
	}
	if (q == 0)
	{
		for (int i = currentmosh.length() - 1; i >= 0; i--)
		{
			if (currentmosh[i] == ')')
			{
				int k = 1;
				for (int j = i - 1; ; j--)
				{
					if (currentmosh[j] == ')')
						k++;
					if (currentmosh[j] == '(')
						k--;
					if (k == 0)
					{
						i = j;
						break;
					}
				}
				continue;
			}
			if (currentmosh[i] == '^')
			{
				string a = "", b = "" ,hole="";
				int k1 = 0, k2 = 0;
				int ee = 0;
				for (int l = i + 1; l < currentmosh.length(); l++)
				{
					if (currentmosh[l] == '(')
						k1++;
					if (currentmosh[l] == ')')
						k1--;
					a += currentmosh[l];
					if (k1 == 0)
						break;
				}
				for (int l = i - 1; l >= 0; l--)
				{
					ee = l;
					if (currentmosh[l] == ')')
						k2++;
					if (currentmosh[l] == '(')
						k2--;
					b += currentmosh[l];
					if (k2 == 0)
						break;
				}
				reverse(b.begin(), b.end());
				hole += '(';
				hole += b;
				hole += '^';
				hole += a;
				hole += ')';
				currentmosh.replace(ee, hole.length() - 2, hole);
				g += 2;
				i -= b.length()-1;
				f--;
				if (f == 1)
				{
					f = 0;
					return 0;
				}
			}
		}
	}
	else
	{
		for (int i = 0; i < currentmosh.length(); i++)
		{
			if (currentmosh[i] == '(')
			{
				int k = 1;
				for (int j = i + 1; ; j++)
				{
					if (currentmosh[j] == '(')
						k++;
					if (currentmosh[j] == ')')
						k--;
					if (k == 0)
					{
						i = j;
						break;
					}
				}
				continue;
			}
			if (q == 1)
			{
				if (currentmosh[i] == '*' || currentmosh[i] == '/')
				{
					string a = "", b = "", hole = "";
					int k1 = 0, k2 = 0;
					int ee = 0;
					for (int l = i + 1; l < currentmosh.length(); l++)
					{
						if (currentmosh[l] == '(')
							k1++;
						if (currentmosh[l] == ')')
							k1--;
						a += currentmosh[l];
						if (k1 == 0)
							break;
					}
					for (int l = i - 1; l >= 0; l--)
					{
						ee = l;
						if (currentmosh[l] == ')')
							k2++;
						if (currentmosh[l] == '(')
							k2--;
						b += currentmosh[l];
						if (k2 == 0)
							break;
					}
					reverse(b.begin(), b.end());
					hole += '(';
					hole += b;
					hole += currentmosh[i];
					hole += a;
					hole += ')';
					currentmosh.replace(ee, hole.length() - 2, hole);
					g += 2;
					i += a.length() + 1;
					f--;
					if (f == 1)
					{
						f = 0;
						return 0;
					}
				}
			}
			if (q == 2)
			{
				if (currentmosh[i] == '+' || currentmosh[i] == '-')
				{
					string a = "", b = "", hole = "";
					int k1 = 0, k2 = 0;
					int ee = 0;
					for (int l = i + 1; l < currentmosh.length(); l++)
					{
						if (currentmosh[l] == '(')
							k1++;
						if (currentmosh[l] == ')')
							k1--;
						a += currentmosh[l];
						if (k1 == 0)
							break;
					}
					for (int l = i - 1; l >= 0; l--)
					{
						ee = l;
						if (currentmosh[l] == ')')
							k2++;
						if (currentmosh[l] == '(')
							k2--;
						b += currentmosh[l];
						if (k2 == 0)
							break;
					}
					reverse(b.begin(), b.end());
					hole += '(';
					hole += b;
					hole += currentmosh[i];
					hole += a;
					hole += ')';
					currentmosh.replace(ee, hole.length() - 2, hole);
					g += 2;
					i += a.length() + 1;
					f--;
					if (f == 1)
					{
						f = 0;
						return 0;
					}
				}
			}
		}
	}
	return check(q + 1);
}

inline int parenthesis(string &currentmosh, int q)
{
	int t = 0;
	int fa, fb;
	bool a = false, b = false;
	if (q == 2)
		return 0;
	if (q == 0)
	{
		for (int i = 0; i < currentmosh.length(); i++)
		{
			if (currentmosh[i] == '(')
			{
				int k = 1;
				for (int z = i + 1; ; z++)
				{
					if (currentmosh[z] == '(')
						k++;
					if (currentmosh[z] == ')')
						k--;
					if (k == 0)
					{
						i = z;
						break;
					}
				}
				continue;
			}
			if (currentmosh[i] == '-' || currentmosh[i] == '+')
			{
				if (i ==0  || currentmosh[i - 1] == '+' || currentmosh[i - 1] == '-' || currentmosh[i - 1] == '*' || currentmosh[i - 1] == '/' || currentmosh[i - 1] == '^' || currentmosh[i - 1] == 's' || currentmosh[i - 1] == 'c' || currentmosh[i - 1] == 'l' || currentmosh[i - 1] == 'q' || currentmosh[i - 1] == 't')
				{
					int y = 0;
					y++;
					string ue = "(";
					ue += currentmosh[i];
					for (int qq = i + 1 ; currentmosh[qq] !='\0' && currentmosh[qq] !='+' && currentmosh[qq] != '-' && currentmosh[qq] != '*' && currentmosh[qq] != '/' && currentmosh[qq] != '^'; qq++)
					{
						if (currentmosh[qq] == '(')
						{
							int k = 1;
							y++;
							ue += currentmosh[qq];
							for (int h = qq + 1; ; h++)
							{
								ue += currentmosh[h];
								if (currentmosh[h] == '(')
									k++;
								if (currentmosh[h] == ')')
									k--;
								y++;
								if (k == 0)
								{
									qq = h;
									break;
								}
							}
							continue;
						}
						ue += currentmosh[qq];
						y++;
					}
					ue += ')';
					currentmosh.replace(i, y, ue);
					g += 2;
					i += y;
				}
			}
		}
	}
	else if (q == 1)
	{
		for (int i = 0; i < currentmosh.length(); i++)
		{
			if (currentmosh[i] == '(')
			{
				t++;
				for (int j = i; j < currentmosh.length(); j++)
				{
					if (currentmosh[j] == ')')
						t--;
					if (t == 0)
					{
						if (currentmosh[j + 1] != '\0')
							i = j + 1;
						else
							i = j;
						break;
					}
				}
			}
			if (currentmosh[i] == '^' || currentmosh[i] == '*' || currentmosh[i] == '/' || currentmosh[i] == '+' || currentmosh[i] == '-')
			{
				int p = i, s = i;
				for (int j = i; currentmosh[j] != '\0' && currentmosh[j] != '(' && currentmosh[j] != '*' && currentmosh[j] != '/' && currentmosh[j] != '+' && currentmosh[j] != '-'; j++)
				{
					if (currentmosh[j] == '^' || currentmosh[j] == '*' || currentmosh[j] == '/' || currentmosh[j] == '+' || currentmosh[j] == '-')
						p = j;
				}
				i = p;
				for (int j = p; j >= s; j--)
				{
					if (currentmosh[j] == ')')
					{
						t++;
						for (int k = j; k >= 0; k--)
						{
							if (currentmosh[k] == '(')
								t--;
							if (t == 0)
							{
								if (k - 1 != -1)
									j = k - 1;
								else
									j = k;
								break;
							}
						}
					}
					if (currentmosh[j] == '^' || currentmosh[j] == '*' || currentmosh[j] == '/' || currentmosh[j] == '+' || currentmosh[j] == '-')
					{
						string catavan = "(", cbtavan = "";
						for (int k = j + 1; k <= currentmosh.length(); k++)
						{
							if (k == currentmosh.length())
							{
								fa = k - 1;
								break;
							}
							if (currentmosh[k] == '(' || currentmosh[k] == '*' || currentmosh[k] == '/' || currentmosh[k] == '+' || currentmosh[k] == '-' || currentmosh[k] == '^')
							{
								if (currentmosh[k] == '(')
									a = true;
								fa = k - 1;
								break;
							}
							catavan += currentmosh[k];
						}
						for (int k = j - 1; k >= -1; k--)
						{
							if (k == -1)
							{
								fb = k + 1;
								break;
							}
							if (currentmosh[k] == ')' || currentmosh[k] == '^' || currentmosh[k] == '/' || currentmosh[k] == '+' || currentmosh[k] == '-' || currentmosh[k] == '*')
							{
								if (currentmosh[k] == ')')
									b = true;
								fb = k + 1;
								break;
							}
							cbtavan += currentmosh[k];
						}
						if (cbtavan != "\0")
						{
							string g = "(";
							reverse(cbtavan.begin(), cbtavan.end());
							g += cbtavan;
							cbtavan = g;
						}
						if (a == true && b == true)
						{
							a = false;
							b = false;
							continue;
						}
						else if (b == true)
						{
							b = false;
							catavan += ')';
							currentmosh.replace(j + 1, fa - j, catavan);
							g += 2;
						}
						else if (a == true)
						{
							a = false;
							cbtavan += ')';
							currentmosh.replace(fb, j - fb, cbtavan);
							g += 2;
							j += 2;
							i += 2;
						}
						else
						{
							if (j == i)
								i += 2;
							else
								i += 4;
							catavan += ')';
							cbtavan += ')';
							currentmosh.replace(j + 1, fa - j, catavan);
							currentmosh.replace(fb, j - fb, cbtavan);
							j += 2;
							g += 4;
						}
					}
				}
			}
		}
	}
	return parenthesis(currentmosh, q + 1);
}

inline int Catch_parentheses(string &mosh, int i)
{
	if (i == mosh.length())
		return 0;
	if (mosh[i] == '(' && F && !visited[i])
	{
		h = i;
		F = false;
		visited[i] = true;
		return i;
	}
	if (F)
	{
		currentmosh += mosh[i];
		return Catch_parentheses(mosh, i - 1);
	}
	if (mosh[i] == ')' && !visited[i])
	{
		F = true;
		Catch_parentheses(mosh, i - 1);
		reverse(currentmosh.begin(), currentmosh.end());
		int q = currentmosh.length();
		parenthesis(currentmosh, 0);
		check();
		mosh.replace(h + 1, q, currentmosh);
		i += g;
		g = 0;
		for (int b = h; b <= i; b++)
			visited[b] = true;
		//cout << currentmosh << endl;
		currentmosh.clear();
	}
	Catch_parentheses(mosh, i + 1);
	return 0;
}

string prefix_postfix(string str , ll num)
{
    if(str.find('-') == -1 && str.find('+') == -1 && str.find('*') == -1 && str.find('/') == -1 && str.find('^')== -1)
        return str;
    ll k =0 ;
    string before ="" , next = "" , after = "";
    char operatorr;
    For(i , 0 , str.size() -1)
    {
        if(str[i] == '(')
            k++;
        if(str[i] == ')')
            k--;
        if(k != 0)
        {
            if(i !=0)
                before += str[i];
            continue;
        }
        if(str[i] == '+' || str[i] == '-' || str[i] == '*' || str[i] == '/' || str[i] == '^')
        {
            operatorr = str[i];
            For(j , i + 2 , str.size()-1)
                after += str[j];
            break;
        }
    }
    if(num == 1)
        return operatorr + prefix_postfix(before , 1)+ prefix_postfix(after , 1) ;
    return prefix_postfix(before , 2)+ prefix_postfix(after, 2) + operatorr ;
}
int main()
{
  //  Test;
	int k = 0 ;
	string mosh;

	getline(cin, mosh);
	for (int i = 0; i < mosh.length(); i++)
	{
		if ((isalpha(mosh[i])) && i != 0)
		{
			if (isdigit(mosh[i - 1]) || isalpha(mosh[i-1]) )
				mosh.insert(i , "*");
		}
	}
	for (int i = 0; i < mosh.length(); i++)
	{
		if (mosh[i] == '(')
			k++;
		if (mosh[i] == ')')
			k--;
		if (k == 0)
		{
			if (i != mosh.length() - 1 || mosh.length() == 1)
			{
				string a = "";
				a += '(';
				a += mosh;
				a += ')';
				mosh = a;
			}
			break;
		}
	}
	Catch_parentheses(mosh, 0);
	mosh.erase(mosh.begin()+0);
	mosh.erase(mosh.end()-1);
    cout << "** prefix ** : " << prefix_postfix(mosh , 1) <<endl;
    cout << "** postfix ** : " << prefix_postfix(mosh , 2) <<endl;
	system("pause");
	return 0;
}
//a+b^c*d+h/f^3^e*g